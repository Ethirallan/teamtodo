<?php echo validation_errors(); ?>

<?php echo form_open(); ?>
    <h3>Admin panel</h3>
    <br>
    <p>
        <b>Admins</b>
        <ul>
            <?php foreach ($admins as $admin): ?>
                <li><?php echo $admin['username']; ?></li>
            <?php endforeach; ?>
        </ul>
        <br>
        <b><label for="addAdmin">Add user</label></b><br>
        <select name="addAdmin">
            <option value="">Select user</option>
            <?php foreach ($users as $user): ?>
                <?php if (!in_array($user, $admins)): ?>
                    <option value="<?php echo $user['id']; ?>"><?php echo $user['username']; ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
        <br>
        <br>
        <b><label for="removeAdmin">Remove user</label></b><br>
        <select name="removeAdmin">
            <option value="">Select user</option>
            <?php foreach ($admins as $admin): ?>
                <option value="<?php echo $admin['id']; ?>"><?php echo $admin['username']; ?></option>
            <?php endforeach; ?>
        </select><br>
        <input class="btn btn-success mt-3" type="submit" name="submit" value="Save changes" />

    </p>
</form>