<div id="main">
	<div id="login">
		<h2>Create new account</h2>
		<hr/>
		<?php
		echo "<div class='error_msg'>";
			echo validation_errors();
		echo "</div>";
		echo form_open('user_authentication/signup');

		echo form_label('Create Username : ');
		echo"<br/>";
		echo form_input('username');
		echo "<div class='error_msg'>";
		
		if (isset($message_display)) {
			echo $message_display;
		}
		echo "</div>";
		echo"<br/>";
		echo form_label('Password : ');
		echo"<br/>";
		echo form_password('password');
		echo"<br/>";
		?>
		<input class="btn btn-success mt-3 mr-2" type="submit" value=" Register " name="submit"/>
		<?php echo form_close(); ?>
		<a class="btn btn-dark" href="<?php echo base_url() ?> ">Go to login</a>
	</div>
</div>