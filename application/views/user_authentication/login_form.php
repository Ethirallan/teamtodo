<?php
if (isset($logout_message)) {
	echo "<div class='message'>";
	echo $logout_message;
	echo "</div>";
}
?>
<?php
if (isset($message_display)) {
	echo "<div class='message'>";
	echo $message_display;
	echo "</div>";
}
?>
<div id="main">
	<div id="login">
		<h2>Welcome back</h2>
		<hr/>
		<?php echo form_open('user_authentication/signin'); ?>
		<?php
		echo "<div class='error_msg'>";
		if (isset($error_message)) {
			echo $error_message;
		}
		echo validation_errors();
		echo "</div>";
		?>
		<label>Username :</label><br>
		<input type="text" name="username" id="name" placeholder="Insert username"/><br /><br />
		<label>Password :</label><br>
		<input type="password" name="password" id="password" placeholder="**********"/><br/><br />
		<input class="btn btn-success mr-2" type="submit" value=" Login " name="submit"/>
		<a class="btn btn-dark" href="<?php echo base_url() ?>index.php/register">Create new account</a>
		<?php echo form_close(); ?>
	</div>
</div>