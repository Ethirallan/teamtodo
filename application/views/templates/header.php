<html>
        <head>
                <title>Team Todo</title>
                <?php $this->load->helper('url'); ?>

                <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
        </head>
        <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-3.5.0.min.js"); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
        <body>
                <div class="navbar navbar-expand-md navbar-dark bg-dark mb-3">
                        <div class="container">
                                <a href="<?php echo site_url('/'); ?>" class="navbar-brand">
                                Team Todo
                                </a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain">
                                        <i class="fa fa-navicon"></i>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarMain">
                                <ul class="navbar-nav ml-auto">
                                
                                        <?php if (isset($this->session->userdata['logged_in'])): ?>
                                                <li class="nav-item">
                                                        <a class="nav-link" style="color: white;">Hello, <?php echo $this->session->userdata['logged_in']['username']; ?></a>
                                                </li>
                                                <li class="nav-item">
                                                        <a href="<?php echo site_url('/projects'); ?>" class="nav-link">My projects</a>
                                                </li>
                                                <?php if (isset($this->session->userdata['is_admin'])): ?>
                                                        <li class="nav-item">
                                                                <a href="<?php echo site_url('/projects/all'); ?>" class="nav-link">All projects</a>
                                                        </li>
                                                        <li class="nav-item">
                                                                <a href="<?php echo site_url('/admins'); ?>" class="nav-link">Admin panel</a>
                                                        </li>
                                                <?php endif; ?>
                                                <li class="nav-item">
                                                        <a href="<?php echo site_url('/logout'); ?>" class="nav-link">Logout</a>
                                                </li>
                                        <?php else: ?>
                                                <li class="nav-item">
                                                        <a href="<?php echo site_url('/login'); ?>" class="nav-link">Login</a>
                                                </li>
                                                <li class="nav-item">
                                                        <a href="<?php echo site_url('/register'); ?>" class="nav-link">Register</a>
                                                </li>
                                        <?php endif; ?>
                                </ul>
                                </div>
                        </div>
                </div>
                <div class="container">