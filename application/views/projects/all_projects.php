<h2><?php echo $title; ?></h2>

<div class="row">
<?php foreach ($projects as $project): ?>
        <div class="col-md-4">
                <div class="card mt-3" style="width: 18rem;">
                        <div class="card-body">
                                <h3><?php echo $project['title']; ?></h3>
                                <p><?php echo $project['date']; ?></p>
                                <a class="btn btn-dark" href="<?php echo site_url('projects/'.$project['id']); ?>">View project</a>
                        </div>
                </div>
        </div>

<?php endforeach; ?>
</div>
<a class="btn btn-success mt-3" href="<?php echo site_url('projects/new/'); ?>">New project</a>