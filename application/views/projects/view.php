<h2><?php echo $title; ?></h2>

<h4>Team members</h4>
<ul>
<?php foreach ($users as $user): ?>
    <li><?php echo $user['username']; ?></li>
<?php endforeach; ?>
</ul>

<button class="btn btn-success mb-3" type="button" onclick="window.location='<?php echo site_url('projects/export/'.$project['id']);?>'">Export project</button>

<h4>Tasks</h4>

<form class="form-inline" action="<?php echo base_url() . 'index.php/projects/'.$project['id']; ?>" method="post">
    <select class="form-control" name="show">
        <option disabled="disabled" value="">Filter By</option>
        <?php if ($type == 'all'): ?>
            <option selected="selected" value="all">All tasks</option>
        <?php else: ?>
            <option value="all">All tasks</option>
        <?php endif; ?>

        <?php if ($type == 'my'): ?>
            <option selected="selected" value="my">My tasks</option>
        <?php else: ?>
            <option value="my">My tasks</option>
        <?php endif; ?>
        <?php if ($type == 'new'): ?>
            <option selected="selected" value="new">New</option>
        <?php else: ?>
            <option value="new">New</option>
        <?php endif; ?>
        <?php if ($type == 'waiting'): ?>
            <option selected="selected" value="waiting">Waiting</option>
        <?php else: ?>
            <option value="waiting">Waiting</option>
        <?php endif; ?>
        <?php if ($type == 'wip'): ?>
            <option selected="selected" value="wip">In progress</option>
        <?php else: ?>
            <option value="wip">In progress</option>
        <?php endif; ?>
        <?php if ($type == 'review'): ?>
            <option selected="selected" value="review">Waiting for a review</option>
        <?php else: ?>
            <option value="review">Waiting for a review</option>
        <?php endif; ?>
        <?php if ($type == 'done'): ?>
            <option selected="selected" value="done">Done</option>
        <?php else: ?>
            <option value="done">Done</option>
        <?php endif; ?>
    </select>
    <input class="btn btn-success ml-2" type="submit" name="filter" value="Go">

<a class="btn btn-success ml-2 mr-2" href="<?php echo site_url('tasks/new/'.$project['id']); ?>">New task</a>
<?php if (isset($this->session->userdata['is_admin'])): ?>
    <a class="btn btn-warning mr-2" href="<?php echo site_url('projects/edit/'.$project['id']); ?>">Edit project</a>
    <a class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this project?')" href="<?php echo site_url('projects/delete/'.$project['id']); ?>">Delete project</a><br>
<?php endif; ?>
</form>

<div class="row">
    <div class="col-2">
        <h5>New</h5>
        <?php foreach ($tasks['new'] as $task): ?>
            <div class="card mt-3">
                    <div class="card-body">
                        <h3><?php echo $task['title']; ?></h3>
                        <p>
                            <b>Date:</b> <?php echo $task['date']; ?>
                            <br>
                            <b>Assigned to:</b> <?php echo $task['username']; ?>
                        </p>
                        <a class="btn btn-dark" href="<?php echo site_url('tasks/'.$task['id']); ?>">View task</a>
                    </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-2">
        <h5>Waiting</h5>
        <?php foreach ($tasks['waiting'] as $task): ?>
            <div class="card mt-3">
                    <div class="card-body">
                        <h3><?php echo $task['title']; ?></h3>
                        <p>
                            <b>Date:</b> <?php echo $task['date']; ?>
                            <br>
                            <b>Assigned to:</b> <?php echo $task['username']; ?>
                        </p>
                        <a class="btn btn-dark" href="<?php echo site_url('tasks/'.$task['id']); ?>">View task</a>
                    </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-2">
        <h5>In progress</h5>
        <?php foreach ($tasks['wip'] as $task): ?>
            <div class="card mt-3">
                    <div class="card-body">
                        <h3><?php echo $task['title']; ?></h3>
                        <p>
                            <b>Date:</b> <?php echo $task['date']; ?>
                            <br>
                            <b>Assigned to:</b> <?php echo $task['username']; ?>
                        </p>
                        <a class="btn btn-dark" href="<?php echo site_url('tasks/'.$task['id']); ?>">View task</a>
                    </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-2">
        <h5>Ready for a review</h5>
        <?php foreach ($tasks['review'] as $task): ?>
            <div class="card mt-3">
                    <div class="card-body">
                        <h3><?php echo $task['title']; ?></h3>
                        <p>
                            <b>Date:</b> <?php echo $task['date']; ?>
                            <br>
                            <b>Assigned to:</b> <?php echo $task['username']; ?>
                        </p>
                        <a class="btn btn-dark" href="<?php echo site_url('tasks/'.$task['id']); ?>">View task</a>
                    </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-2">
        <h5>Done</h5>
        <?php foreach ($tasks['done'] as $task): ?>
            <div class="card mt-3">
                    <div class="card-body">
                        <h3><?php echo $task['title']; ?></h3>
                        <p>
                            <b>Date:</b> <?php echo $task['date']; ?>
                            <br>
                            <b>Assigned to:</b> <?php echo $task['username']; ?>
                        </p>
                        <a class="btn btn-dark" href="<?php echo site_url('tasks/'.$task['id']); ?>">View task</a>
                    </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>