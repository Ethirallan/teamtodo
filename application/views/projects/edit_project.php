<?php echo validation_errors(); ?>

<?php echo form_open(); ?>

    <b><label for="title">Title</label></b><br>
    <input type="input" name="title" value="<?php echo $project['title']; ?>"></input><br /><br>

    <b><label for="users">Team members</label></b><br>
    <?php echo form_multiselect('users[]', $users); ?>
    <br>

    <input class="btn btn-dark mt-3" type="submit" name="submit" value="Save changes" />

</form>