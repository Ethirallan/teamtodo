<h3><?php echo $task['title']; ?></h3>
<p>
    <b>Date: </b><br><?php echo $task['date']; ?><br>
    <b>Status: </b> <br><?php echo $status; ?><br>
    <b>Assigned to: </b> <br><?php echo $task['user']; ?><br>
    <b>Description: </b> <br><?php echo $task['description']; ?><br>
</p>

<a class="btn btn-warning mt-3 mr-2" href="<?php echo site_url('tasks/edit/'.$task['id']); ?>">Edit task</a>
<a class="btn btn-danger mt-3" onclick="return confirm('Are you sure you want to delete this task?')" href="<?php echo site_url('tasks/delete/'.$task['id'].'/'.$task['project_id']); ?>">Delete task</a><br>