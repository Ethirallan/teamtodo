<?php echo validation_errors(); ?>

<?php echo form_open(); ?>

    <b><label for="title">Title</label></b><br>
    <input type="input" name="title" value="<?php echo $task['title']; ?>"></input><br /><br>

    <b><label for="status">Status</label></b><br>
    <select name="status">
        <?php foreach ($statuses as $status): ?>
            <?php if ($task['status_id'] == $status['id']): ?>
                <option selected value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
            <?php else: ?>
                <option value="<?php echo $status['id']; ?>"><?php echo $status['name']; ?></option>
            <?php endif; ?>
        <?php endforeach; ?>
    </select>
    <br>
    <b><label for="assignedTo">Assigned to</label></b><br>
    <select name="assignedTo">
        <?php foreach ($users as $user): ?>
            <option value="<?php echo $user['id']; ?>"><?php echo $user['username']; ?></option>
        <?php endforeach; ?>
    </select><br><br>

    <b><label for="text">Description</label></b><br>
    <textarea name="description"><?php echo $task['description']; ?></textarea><br /><br>

    <input class="btn btn-dark" type="submit" name="submit" value="Save changes" />

</form>