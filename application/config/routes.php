<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes with
| underscores in the controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['projects/new'] = 'projects/new_project';
$route['projects/edit/(:any)'] = 'projects/edit_project/$1';
$route['projects/delete/(:any)'] = 'projects/delete_project/$1';
$route['projects'] = 'projects';
$route['projects/all'] = 'projects/all_projects';
$route['projects/export/(:any)'] = 'projects/export/$1';
$route['projects/(:any)'] = 'projects/view/$1';
$route['tasks/(:any)'] = 'projects/task/$1';
$route['tasks/new/(:any)'] = 'projects/new_task/$1';
$route['tasks/edit/(:any)'] = 'projects/edit_task/$1';
$route['tasks/delete/(:any)/(:any)'] = 'projects/delete_task/$1/$2';
$route['admins'] = 'user_authentication/admin_panel';
$route['login'] = 'user_authentication';
$route['register'] = 'user_authentication/register';
$route['logout'] = 'user_authentication/logout';
$route['default_controller'] = 'projects';