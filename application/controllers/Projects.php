<?php
class Projects extends Guard_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('projects_model');
                $this->load->helper('url_helper');
                $this->load->library('session');
                $this->load->helper('file');
        }


        public function index() {
                $data['projects'] = $this->projects_model->get_user_projects($this->session->userdata['logged_in']['id']);
                $data['title'] = 'My projects';
        
                $this->load->view('templates/header', $data);
                $this->load->view('projects/index', $data);
                $this->load->view('templates/footer');
        }

        public function all_projects() {
                $data['projects'] = $this->projects_model->get_all_projects();
                $data['title'] = 'All projects';
        
                $this->load->view('templates/header', $data);
                $this->load->view('projects/all_projects', $data);
                $this->load->view('templates/footer');
        }

        public function view($id) {

                $show = $this->input->post('show');
                $data['type'] = 'all';
                if ($show == 'my') {
                        $data['type'] = 'my';
                        $user_id = $this->session->userdata['logged_in']['id'];
                        $data['tasks']['new'] = $this->projects_model->get_tasks_by_user_id($id, 1, $user_id);
                        $data['tasks']['done'] = $this->projects_model->get_tasks_by_user_id($id, 2, $user_id);
                        $data['tasks']['wip'] = $this->projects_model->get_tasks_by_user_id($id, 3, $user_id);
                        $data['tasks']['review'] = $this->projects_model->get_tasks_by_user_id($id, 4, $user_id);
                        $data['tasks']['waiting'] = $this->projects_model->get_tasks_by_user_id($id, 5, $user_id);
                } else if ($show == 'all' || $show == '') {
                        $data['type'] = 'all';
                        $data['tasks']['new'] = $this->projects_model->get_tasks($id, 1);
                        $data['tasks']['done'] = $this->projects_model->get_tasks($id, 2);
                        $data['tasks']['wip'] = $this->projects_model->get_tasks($id, 3);
                        $data['tasks']['review'] = $this->projects_model->get_tasks($id, 4);
                        $data['tasks']['waiting'] = $this->projects_model->get_tasks($id, 5);
                } else if ($show == 'new') {
                        $data['type'] = 'new';
                        $data['tasks']['new'] = $this->projects_model->get_tasks($id, 1);
                        $data['tasks']['done'] = array();
                        $data['tasks']['wip'] = array();
                        $data['tasks']['review'] = array();
                        $data['tasks']['waiting'] = array();
                } else if ($show == 'done') {
                        $data['type'] = 'done';
                        $data['tasks']['done'] = $this->projects_model->get_tasks($id, 1);
                        $data['tasks']['new'] = array();
                        $data['tasks']['wip'] = array();
                        $data['tasks']['review'] = array();
                        $data['tasks']['waiting'] = array();
                } else if ($show == 'waiting') {
                        $data['type'] = 'waiting';
                        $data['tasks']['waiting'] = $this->projects_model->get_tasks($id, 1);
                        $data['tasks']['done'] = array();
                        $data['tasks']['wip'] = array();
                        $data['tasks']['review'] = array();
                        $data['tasks']['new'] = array();
                } else if ($show == 'review') {
                        $data['type'] = 'review';
                        $data['tasks']['review'] = $this->projects_model->get_tasks($id, 1);
                        $data['tasks']['done'] = array();
                        $data['tasks']['wip'] = array();
                        $data['tasks']['new'] = array();
                        $data['tasks']['waiting'] = array();
                } else if ($show == 'wip') {
                        $data['type'] = 'wip';
                        $data['tasks']['wip'] = $this->projects_model->get_tasks($id, 1);
                        $data['tasks']['done'] = array();
                        $data['tasks']['new'] = array();
                        $data['tasks']['review'] = array();
                        $data['tasks']['waiting'] = array();
                }

                $data['project'] = $this->projects_model->get_project($id);
                $data['users'] = $this->projects_model->get_users_on_project($id);

                if (empty($data['project']))
                {
                        show_404();
                }

                $data['title'] = $data['project']['title'];

                $this->load->view('templates/header', $data);
                $this->load->view('projects/view', $data);
                $this->load->view('templates/footer');
        }

        public function delete_project($task_id, $project_id){
                $this->projects_model->delete_project($task_id);        
                redirect( base_url() . 'index.php/projects/');        
        }

        public function task($id) {
                $data['task'] = $this->projects_model->get_task($id);
                $data['task']['user'] = $this->projects_model->get_username($data['task']['user_id']);
                $data['status'] = $this->projects_model->get_status_name($data['task']['status_id']);
                if (empty($data['task']))
                {
                        show_404();
                }

                $this->load->view('templates/header', $data);
                $this->load->view('projects/task_details', $data);
                $this->load->view('templates/footer');
        }

        public function new_task($project_id) {
                $this->load->helper('form');
                $this->load->library('form_validation');

                $data['statuses'] = $this->projects_model->get_statuses();
                $data['users'] = $this->projects_model->get_users_on_project($project_id);

                $this->form_validation->set_rules('title', 'Title', 'required');
                $this->form_validation->set_rules('status', 'Status', 'required');
                $this->form_validation->set_rules('assignedTo', 'AssignedTo', 'required');
                $this->form_validation->set_rules('description', 'Description', 'required');
 
                
                if ($this->form_validation->run() === FALSE) {
                        $this->load->view('templates/header');
                        $this->load->view('projects/new_task', $data);
                        $this->load->view('templates/footer');
                } else {
                        $this->projects_model->add_task($project_id); 
                        redirect( base_url() . 'index.php/projects/'.$project_id);        
                        return;
                }
        }

        public function delete_task($task_id, $project_id){
            $this->projects_model->delete_task($task_id);        
            redirect( base_url() . 'index.php/projects/'.$project_id);        
        }

        public function edit_task($task_id) {
                $this->load->helper('form');
                $this->load->library('form_validation');

                $data['task'] = $this->projects_model->get_task($task_id);
                $data['users'] = $this->projects_model->get_users_on_project($data['task']['project_id']);
                $data['statuses'] = $this->projects_model->get_statuses();
                $this->form_validation->set_rules('title', 'Title', 'required');
                $this->form_validation->set_rules('status', 'Status', 'required');
                $this->form_validation->set_rules('assignedTo', 'AssignedTo', 'required');
                $this->form_validation->set_rules('description', 'Description', 'required');
 
                
                if ($this->form_validation->run() === FALSE)
                {
                        $this->load->view('templates/header');
                        $this->load->view('projects/edit_task', $data);
                        $this->load->view('templates/footer');

                } else {
                        $this->projects_model->update_task($task_id, $data['task']['project_id']); 
                        redirect( base_url() . 'index.php/projects/'.$data['task']['project_id']);        
                        return;
                }
        }

        public function new_project() {
                $this->load->helper('form');
                $this->load->library('form_validation');

                $users_res = $this->projects_model->get_all_users();

                $users = array();
                foreach ($users_res as $row) {
                        $users[$row['id']] = $row['username'];
                }

                $data['users'] = $users;

                $this->form_validation->set_rules('title', 'Title', 'required');
                
                if ($this->form_validation->run() === FALSE) {
                        $this->load->view('templates/header');
                        $this->load->view('projects/new_project', $data);
                        $this->load->view('templates/footer');
                } else {
                        $project_id = $this->projects_model->add_project(); 
                        $selected_users = $this->input->post('users');
                        foreach ($selected_users as $user) {
                                $this->projects_model->add_user_to_project($user, $project_id);
                        }
                        redirect( base_url() . 'index.php/projects');        
                        return;
                }
        }

        public function edit_project($project_id) {
                $this->load->helper('form');
                $this->load->library('form_validation');

                $old_users = $this->projects_model->get_users_on_project($project_id);
                $users_res = $this->projects_model->get_all_users();

                $users = array();
                foreach ($users_res as $row) {
                        $users[$row['id']] = $row['username'];
                }

                $data['users'] = $users;
                $data['project'] = $this->projects_model->get_project($project_id);

                $this->form_validation->set_rules('title', 'Title', 'required');
                
                if ($this->form_validation->run() === FALSE) {
                        $this->load->view('templates/header');
                        $this->load->view('projects/edit_project', $data);
                        $this->load->view('templates/footer');
                } else {
                        $this->projects_model->update_project($project_id); 
                        $selected_users = $this->input->post('users');
                        echo '<br>';
                        echo '<br>';


                        print_r($selected_users); // ids
                        echo '<br>';
                        print_r($users_res); 
                        echo '<br>';
                        print_r($old_users);

                        if (count($selected_users) > 0) {
                                foreach ($users_res as $user) {
                                        if ($this->isPart($user['id'], $old_users) && !in_array($user['id'], $selected_users)) {
                                                $this->projects_model->remove_user_from_project($user['id'], $project_id);
                                        } else if (!$this->isPart($user['id'], $old_users) && in_array($user['id'], $selected_users)) {
                                                $this->projects_model->add_user_to_project($user['id'], $project_id);
                                        }
                                }
                        } else {
                                foreach ($old_users as $user) {
                                        $this->projects_model->remove_user_from_project($user['id'], $project_id);
                                }
                        }


                        redirect( base_url() . 'index.php/projects/'.$project_id);        
                        return;
                }
        }

        public function isPart($id, $arr) {
                foreach ($arr as $el) {
                        if ($id == $el['id']) {
                                return true;
                        }
                }
                return false;
        }

        public function export($project_id) {
                $sql['project_info'] = $this->projects_model->get_project($project_id);
                $sql['tasks'] = $this->projects_model->get_all_tasks($project_id);
                $sql['users'] = $this->projects_model->get_users_on_project($project_id);
                echo json_encode($sql, TRUE);
        }
}