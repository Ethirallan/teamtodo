<?php

Class User_authentication extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper('url_helper');

        $this->load->helper('form');

        $this->load->library('form_validation');

        $this->load->library('session');

        $this->load->model('login_database');

        $this->load->library('session');
    }

    public function index() {
        $this->load->view('templates/header');
        $this->load->view('user_authentication/login_form');
        $this->load->view('templates/footer');
    }

    public function register() {
        $this->load->view('templates/header');
        $this->load->view('user_authentication/registration_form');
        $this->load->view('templates/footer');
    }

    public function signup() {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('user_authentication/registration_form');
        } else {
            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
            );
            $result = $this->login_database->registration_insert($data);
            if ($result == TRUE) {
                $data['message_display'] = 'Registration Successfully !';
                $this->load->view('templates/header');
                $this->load->view('user_authentication/login_form', $data);
                $this->load->view('templates/footer');
            } else {
                $this->load->view('templates/header');
                $this->load->view('user_authentication/registration_form');
                $this->load->view('templates/footer');
            }
        }
    }

    // Check for user login process
    public function signin() {

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );
        $user = $this->login_database->login($data);

        if (count($user) > 0) {

            $username = $this->input->post('username');

            $is_admin = $this->login_database->is_admin($user['id']);
            $session_data = array(
                'username' => $user['username'],
                'id' => $user['id']
            );

            $data = array('error_message' => 'Signin OK');
            $this->session->set_userdata('logged_in', $session_data);
            if ($is_admin) {
                $this->session->set_userdata('is_admin', $session_data);
            }
            redirect( base_url() . 'index.php/projects/');        
        } else {
            $data = array(
                'error_message' => 'Invalid Username or Password'
            );
            $this->load->view('templates/header');
            $this->load->view('user_authentication/login_form', $data);
            $this->load->view('templates/footer');
        }
    }

    public function logout() {
        $sess_array = array(
            'username' => '',
            'id' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        $this->session->unset_userdata('is_admin', $sess_array);
        $data['message_display'] = 'Successfully Logout';
        $this->load->view('templates/header');
        $this->load->view('user_authentication/login_form', $data);
        $this->load->view('templates/footer');
    }

    public function admin_panel() {
        if ( !$this->session->userdata('is_admin'))
        { 
            redirect('/');
        }
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('addAdmin', 'addAdmin', 'callback_admin_val');
        $this->form_validation->set_rules('removeAdmin', 'removeAdmin', 'callback_admin_val');


        if ($this->form_validation->run() === FALSE) {
            $data['admins'] = $this->login_database->get_admins();
            $data['users'] = $this->login_database->get_all_users();
            $this->load->view('templates/header');
            $this->load->view('user_authentication/admin_panel', $data);
            $this->load->view('templates/footer');

        } else {
                if ($this->input->post('addAdmin') != '') {
                    $this->login_database->add_admin();
                }
                if ($this->input->post('removeAdmin') != '') {
                    $this->login_database->remove_admin();
                }
                redirect( base_url() . 'index.php/admins/');        
                return;
        }
    }

    function admin_val() {
        $add = $this->input->post('addAdmin');
        $remove = $this->input->post('removeAdmin');
        if(empty($add) && empty($remove)){// check empty of both filed
         $this->form_validation->set_message("check_validation", 'Select at lear one user');
         return FALSE;
        } else {
         return TRUE;
       }
     }
}

?>