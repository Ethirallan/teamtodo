<?php

Class Login_database extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	public function registration_insert($data) {

		$condition = "username =" . "'" . $data['username'] . "'";
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {

			$this->db->insert('users', $data);
			if ($this->db->affected_rows() > 0) {
				return true;
			}
		} else {
			return false;
		}
	}

	public function login($data) {

		$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "'";
		$this->db->select('id, username');
		$this->db->from('users');
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function is_admin($id) {
		$this->db->select('*');
		$this->db->from('admin_group');
		$this->db->where('user_id', $id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function get_all_users() {
        $this->db->select('id, username');
		$query = $this->db->get('users');
		return $query->result_array();
	}
	
	public function get_admins() {
		$this->db->select('users.id, users.username');
		$this->db->from('users');
		$this->db->join('admin_group', 'admin_group.user_id = users.id');
		$query = $this->db->get();
		return $query->result_array();
	}
	

    public function add_admin() {
        $data = array(
			'user_id' => $this->input->post('addAdmin')
        );
        $this->db->insert('admin_group', $data);
    }

    public function remove_admin() {
        $this->db->where('user_id', $this->input->post('removeAdmin'));
        $this->db->delete('admin_group');
    }
}

?>