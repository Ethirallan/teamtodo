<?php
class projects_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_user_projects($user_id) {
        $this->db->select('projects.id, projects.title, projects.date');
        $this->db->join('project_link', 'projects.id = project_link.project_id', 'left');
        $this->db->where('project_link.user_id', $user_id);
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function get_project($project_id) {
        $query = $this->db->get_where('projects', array('id' => $project_id));
        return $query->result_array()[0];
    }

    public function get_all_projects() {
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function delete_project($project_id){
        $this->db->where('id', $project_id);
        return $this->db->delete('projects');
    }

    public function add_project() {
        $data = array(
            'title' => $this->input->post('title'),
            'date' => date('Y-m-d')
        );
        $this->db->insert('projects', $data);
        return $this->db->insert_id();
    }

    public function update_project($project_id) {
        $data = array(
            'title' => $this->input->post('title'),
            'date' => date('Y-m-d')
        );
        $this->db->where('id', $project_id);
        $this->db->update('projects', $data);
    }

    public function add_teammate($project_id, $user_id) {
        $data = array(
            'project_id' => $project_id,
            'user_id' => $user_id
        );
        $this->db->replace('project_link', $data);
    }

    public function remove_teammate($project_id, $user_id) {
        $this->db->where('project_id', $project_id);
        $this->db->where('user_id', $user_id);
        return $this->db->delete('project_link');
    }

    public function get_tasks($project_id, $status_id) {
        $this->db->select('tasks.*, users.username');
        $this->db->where('project_id', $project_id);
        $this->db->where('status_id', $status_id);
        $this->db->join('users', 'users.id = tasks.user_id', 'left');
        $query = $this->db->get('tasks');
        return $query->result_array();
    }

    public function get_all_tasks($project_id) {
        $this->db->select('tasks.*, users.username');
        $this->db->where('project_id', $project_id);
        $this->db->join('users', 'users.id = tasks.user_id', 'left');
        $query = $this->db->get('tasks');
        return $query->result_array();
    }

    public function get_task($task_id) {
        $query = $this->db->get_where('tasks', array('id' => $task_id));
        return $query->row_array();
    }

    public function get_tasks_by_user_id($project_id, $status_id, $user_id) {
        $this->db->select('tasks.*, users.username');
        $this->db->where('project_id', $project_id);
        $this->db->where('status_id', $status_id);
        $this->db->where('users.id', $user_id);
        $this->db->join('users', 'users.id = tasks.user_id', 'left');
        $query = $this->db->get('tasks');
        return $query->result_array();
    }

    public function get_user_tasks($user_id) {
        $query = $this->db->get_where('tasks', array('user_id' => $user_id));
        return $query->result_array();
    }

    public function add_task($project_id) {
        $data = array(
            'title' => $this->input->post('title'),
            'project_id' => $project_id,
            'status_id' => $this->input->post('status'),
            'user_id' => $this->input->post('assignedTo'),
            'description' => $this->input->post('description'),
            'date' => date('Y-m-d')
        );
        $this->db->insert('tasks', $data);
    }

    public function update_task($task_id, $project_id) {
        $data = array(
            'title' => $this->input->post('title'),
            'project_id' => $project_id,
            'status_id' => $this->input->post('status'),
            'user_id' => $this->input->post('assignedTo'),
            'description' => $this->input->post('description'),
            'date' => date('Y-m-d')
        );

        $this->db->where('id', $task_id);
        $this->db->update('tasks', $data);
    }

    public function delete_task($task_id){
        $this->db->where('id', $task_id);
        return $this->db->delete('tasks');
    }

	public function get_all_users() {
        $this->db->select('id, username');
		$query = $this->db->get('users');
		return $query->result_array();
    }
    
    public function get_users_on_project($project_id) {
        $this->db->select('users.id, users.username');
        $this->db->from('project_link');
        $this->db->join('users', 'users.id = project_link.user_id', 'left');
        $query = $this->db->where('project_id', $project_id)->get();
        return $query->result_array();
    }

    public function add_user_to_project($user_id, $project_id) {
        $data = array(
            'user_id' => $user_id,
            'project_id' => $project_id
        );
        $this->db->insert('project_link', $data);
    }

    public function remove_user_from_project($user_id, $project_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('project_id', $project_id);
        $this->db->delete('project_link');
    }

    public function get_username($user_id) {
        $this->db->select('username');
        $this->db->where('id', $user_id);
        $query = $this->db->get('users');
        return $query->row_array()['username'];
    }

    public function get_statuses() {
        $query = $this->db->get('status');
        return $query->result_array();
    }

    public function get_status_name($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('status');
        return $query->row_array()['name'];
    }
}