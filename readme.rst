###################
Team todo
###################

Application for managing team work. Administrators can view everything, create, edit and delete projects and add users to admin 
group (or remove them). Other can register and login, and view their projects. For every project they can add, edit and delete task.
Tasks can also be filtered or exported (JSON).
